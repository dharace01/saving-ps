package com.app.savingps.pref

import com.app.savingps.model.User
import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.gsonpref.gsonNullablePref

object AppPref : KotprefModel() {

    fun clearPref() {
        clear()
    }

    var IsLogin by booleanPref(false)
    var User by gsonNullablePref<User>(User())
    var code by stringPref("")



    var isLoginUpdate by booleanPref(false)
    var isLocationSuccess by booleanPref(false)

    var share_code by stringPref()
    var age by stringPref()
    var appLocaleCode by stringPref("en")
    var isLangSelected by booleanPref()
    var isCompletedSlide by booleanPref()
    var latitude by stringPref("0.0")
    var longitude by stringPref("0.0")
    var recent by gsonNullablePref<ArrayList<String>>(ArrayList())

}
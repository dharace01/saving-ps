package com.app.savingps.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_forget_pin.*
import org.jetbrains.anko.intentFor
import org.json.JSONObject

class ForgetPinActivity : BaseActivity(R.layout.activity_forget_pin) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {

        otpView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 6) {
                    otpVerifyApi(s)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun otpVerifyApi(s: CharSequence) {

        val jsonObject = JSONObject()
        jsonObject.put("code", s.toString())
        jsonObject.put("auth_token", intent.getStringExtra("token"))

        callApi(apiClient.postVerifyOtp(jsonObject), true) {
            if(it.User != null){
                startActivity(intentFor<ResetLoginPinActivity>().putExtra("token", it.User.authToken))
            }
            sneakerError(it.status!!)
        }
    }


}
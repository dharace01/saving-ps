package com.app.savingps.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_pot_success.*
import kotlinx.android.synthetic.main.activity_pot_success.btnOk
import kotlinx.android.synthetic.main.activity_topup_success.*

class TopupSuccessActivity : BaseActivity(R.layout.activity_topup_success) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tvMsg.text = intent.getStringExtra("msg")
        btnOk.setOnClickListener {
            finish()
        }
    }
}
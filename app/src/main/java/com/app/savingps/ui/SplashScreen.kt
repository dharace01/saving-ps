package com.app.savingps.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.app.savingps.pref.AppPref
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class SplashScreen : BaseActivity(R.layout.activity_splash_screen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    //  +923145803875
//    d68e4b70-0524-4728-b247-449619191e74
// john doe

    private fun initView() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (AppPref.IsLogin) {
               Log.e("code", AppPref.code);
               Log.e("user", AppPref.User.toString())
                startActivity(intentFor<DashboardActivity>().clearTask().newTask())
            } else {
                startActivity(intentFor<IntroActivity>().clearTask().newTask())
            }
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }, 3000)
    }
}
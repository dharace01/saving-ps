package com.app.savingps.ui

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.savingps.R
import com.app.savingps.adapters.SpendingAdapter
import com.app.savingps.adapters.SpendingDetailsAdapter
import com.app.savingps.base.BaseActivity
import com.app.savingps.model.SpendingData
import com.app.savingps.model.SpendingDetailData
import kotlinx.android.synthetic.main.activity_pot_success.*
import kotlinx.android.synthetic.main.activity_spending_detail.*
import kotlinx.android.synthetic.main.activity_spending_detail.list
import kotlinx.android.synthetic.main.fragment_insight.*
import kotlinx.android.synthetic.main.fragment_insight.view.*
import org.jetbrains.anko.intentFor

class SpendingDetailActivity : BaseActivity(R.layout.activity_spending_detail) {

    val listData: ArrayList<SpendingDetailData> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {

        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "Virgin media", "£1,55.90"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "Virgin mobile", "£55.590"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "sky", "£1,05.590"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "EDF energy", "£55.0"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "Vodafone", "£1,055.0"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "Virgin media", "£1,055.0"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "Virgin mobile", "£15.590"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "sky", "£5.590"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "EDF energy", "£15.0"))
        listData.add(SpendingDetailData(intent.getIntExtra("img", 0), "Vodafone", "£155.5"))

        tvTitle.text = intent.getStringExtra("title")
        list.adapter = SpendingDetailsAdapter().also {
            it.addAll(listData)
           list.layoutManager = LinearLayoutManager(this)
            it.setItemClickListener { view, position, item ->
                when (view.id) {
                    R.id.layout -> {

                    }
                }
            }
        }

        ivBack.setOnClickListener {
            finish()
        }

    }
}
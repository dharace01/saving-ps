package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_update_details.*
import org.jetbrains.anko.intentFor

class UpdateDetailsActivity : BaseActivity(R.layout.activity_update_details), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOnClick()
    }

    private fun initOnClick() {
        btnUpdate.setOnClickListener(this)
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnUpdate -> {
                finish()
            }

            R.id.ivBack -> {
               onBackPressed()
            }

        }
    }
}
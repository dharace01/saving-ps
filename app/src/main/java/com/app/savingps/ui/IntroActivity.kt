package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import androidx.core.text.HtmlCompat
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.teresaholfeld.stories.StoriesProgressView
import kotlinx.android.synthetic.main.activity_intro.*
import org.jetbrains.anko.intentFor


class IntroActivity : BaseActivity(R.layout.activity_intro), StoriesProgressView.StoriesListener {

    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()

    }

    private fun initView() {

        text.text = HtmlCompat.fromHtml(getString(R.string.test), HtmlCompat.FROM_HTML_MODE_LEGACY)

        stories.setStoriesCount(PROGRESS_COUNT)
        stories.setStoryDuration(5000L)
        stories?.setStoriesListener(this)

        counter = 0
        stories.startStories(counter)
        setLayout()

        layout.setOnClickListener {
            startActivity(intentFor<LoginActivity>())
            finish()
        }
    }

    private fun setLayout() {
        if (counter == 0) {
            layout1.visibility = View.VISIBLE
            layout2.visibility = View.GONE
        } else {
            layout1.visibility = View.GONE
            layout2.visibility = View.VISIBLE
        }
    }

    override fun onComplete() {
        startActivity(intentFor<LoginActivity>())
        finish()
    }

    override fun onNext() {
        ++counter
        setLayout()
    }

    override fun onPrev() {
        if (counter - 1 < 0) return
        --counter
        setLayout()
    }

    override fun onDestroy() {
        // Very important !
        stories?.destroy()
        super.onDestroy()
    }

    companion object {
        private const val PROGRESS_COUNT = 2
    }
}
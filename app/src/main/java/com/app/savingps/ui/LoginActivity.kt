package com.app.savingps.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.app.savingps.model.User
import com.app.savingps.pref.AppPref
import com.app.savingps.util.isValidEmail
import com.app.savingps.util.isValidPhoneNumber
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import org.jetbrains.anko.intentFor
import org.json.JSONObject


class LoginActivity : BaseActivity(R.layout.activity_login), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

        edPhone.setText(intent.getStringExtra("phone")?.replace("+92", ""))
        otpView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 6) {
                    callLogin(s)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isValidate(): Boolean {
        return when {

            TextUtils.isEmpty(edPhoneCode.text.toString().trim()) -> {
                edPhoneCode.error = "Please enter Phone code"
                false
            }

            !isValidPhoneNumber(edPhone.text.toString().trim()) -> {
                edPhone.error = "Please enter valid Phone number!"
                false
            }

            else -> {
                true
            }
        }
    }

    private fun callLogin(s: CharSequence) {

        val jsonObject = JSONObject()
        jsonObject.put("pin", s.toString())
        jsonObject.put("phone", edPhoneCode.text.toString() + edPhone.text)

        callApi(apiClient.postCallLogin(jsonObject), true) {
            if (it.User != null) {
                AppPref.User = it.User
                if(it.is_code_needed!!){
                    startActivity(intentFor<WebviewActivity>())
                }else{
                  //  startActivity(intentFor<ExpensesActivity>())
                    AppPref.IsLogin = true
                    startActivity(intentFor<DashboardActivity>())
                }
            }
            sneakerError(it.status!!)
        }
    }

    private fun initOnClick() {
        tvSignup.setOnClickListener(this)
        tvForgotPin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.tvSignup -> startActivity(intentFor<SignupActivity>())
            R.id.tvForgotPin -> if (isValidate()) callForgetPin()
        }
    }

    private fun callForgetPin() {
        val jsonObject = JSONObject()
        jsonObject.put("phone", edPhoneCode.text.toString() + edPhone.text)

        callApi(apiClient.postForgetPin(jsonObject), true) {
            if (it.User != null) {
                startActivity(intentFor<ForgetPinActivity>().putExtra("token", it.User.authToken))
            }
            sneakerError(it.status!!)
        }
    }
}
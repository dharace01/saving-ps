package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.savingps.R
import com.app.savingps.adapters.ExistingContactsAdapter
import com.app.savingps.adapters.PaymentHistoryAdapter
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_pay_existing_contact.*
import org.jetbrains.anko.intentFor

class PayExistingContactActivity : BaseActivity(R.layout.activity_pay_existing_contact),
    View.OnClickListener {

    val listData: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        listData.add("1");
        listData.add("1");
        listData.add("1");

        initView()
        initOnClick()
    }

    private fun initView() {

        tvExisting.setTextColor(ContextCompat.getColor(this, R.color.colorTitle))
        tvPaymentHistory.setTextColor(ContextCompat.getColor(this, R.color.colorTitle1))

        viewExisting.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTitle))
        viewPaymentHistory.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTitle1))

        listExisting.visibility = View.VISIBLE
        listHistory.visibility = View.GONE

        listExisting.adapter = ExistingContactsAdapter().also {
            it.addAll(listData)
            listExisting.layoutManager = LinearLayoutManager(this)
            it.setItemClickListener { view, position, image ->
                when (view.id) {
                    R.id.layout -> {
                        startActivity(
                            intentFor<TransferMoneyFormActivity>()
                                .putExtra("title", "Existing Contact")
                                .putExtra("from", 0)
                        )
                    }
                }
            }
        }

        listHistory.adapter = PaymentHistoryAdapter().also {
            it.addAll(listData)
            listHistory.layoutManager = LinearLayoutManager(this)
            it.setItemClickListener { view, position, image ->
                when (view.id) {
                    R.id.layout -> {
                        startActivity(
                            intentFor<TransferMoneyFormActivity>()
                                .putExtra("title", "Repeat payment")
                                .putExtra("from", 1)
                        )
                    }
                }
            }
        }

    }


    private fun initOnClick() {
        icBack.setOnClickListener(this)
        rlExistingContact.setOnClickListener(this)
        rlPaymentHistory.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.icBack -> {
                onBackPressed()
            }
            R.id.rlExistingContact -> {
                tvExisting.setTextColor(ContextCompat.getColor(this, R.color.colorTitle))
                tvPaymentHistory.setTextColor(ContextCompat.getColor(this, R.color.colorTitle1))

                viewExisting.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTitle))
                viewPaymentHistory.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTitle1
                    )
                )

                listExisting.visibility = View.VISIBLE
                listHistory.visibility = View.GONE
            }
            R.id.rlPaymentHistory -> {
                tvExisting.setTextColor(ContextCompat.getColor(this, R.color.colorTitle1))
                tvPaymentHistory.setTextColor(ContextCompat.getColor(this, R.color.colorTitle))

                viewExisting.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTitle1))
                viewPaymentHistory.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTitle
                    )
                )

                listExisting.visibility = View.GONE
                listHistory.visibility = View.VISIBLE
            }
        }
    }
}
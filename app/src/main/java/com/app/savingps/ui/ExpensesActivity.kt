package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_expenses.*
import org.jetbrains.anko.intentFor

class ExpensesActivity : BaseActivity(R.layout.activity_expenses), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        progressBills.startProgressAnimation()
        progressGrocery.startProgressAnimation()
        progressEntertain.startProgressAnimation()
    }

    private fun initOnClick() {
        btnNext.setOnClickListener(this)
//        tvForgotPin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnNext -> {
                startActivity(intentFor<Expenses2Activity>())
            }
        }
    }
}
package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_pot_detail.*
import org.jetbrains.anko.intentFor

class PotDetailActivity : BaseActivity(R.layout.activity_pot_detail), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        progress.progress = 50F
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        btnEditPot.setOnClickListener(this)
        btnDeletePot.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.btnEditPot -> {
                startActivity(intentFor<EditPotActivity>())
            }
            R.id.btnDeletePot -> {
                finish()
            }
        }
    }

}
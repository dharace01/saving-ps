package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_monthly_recurring_savings.icBack
import kotlinx.android.synthetic.main.activity_transfer_money.*
import org.jetbrains.anko.intentFor

class TransferMoneyActivity : BaseActivity(R.layout.activity_transfer_money) ,
    View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOnClick()
    }

    private fun initOnClick() {
        icBack.setOnClickListener(this)
        btnTransferMoney.setOnClickListener(this)
        btnPayExistingContact.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.icBack -> {
                onBackPressed()
            }
            R.id.btnTransferMoney -> {
                startActivity(
                    intentFor<TopupSuccessActivity>().putExtra(
                        "msg",
                        "Transfer\nsuccessful"
                    )
                )
                finish()
            }
            R.id.btnPayExistingContact -> {
                startActivity(intentFor<PayExistingContactActivity>())
            }

        }
    }

}
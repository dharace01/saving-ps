package com.app.savingps.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.app.savingps.model.Categories
import com.app.savingps.model.Tip
import com.app.savingps.pref.AppPref
import com.app.savingps.util.HideDialog
import com.app.savingps.util.ShowDialog
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.printing.partner.fragments.AddFragment
import com.printing.partner.fragments.InsightFragment
import com.printing.partner.fragments.MyAccountFragment
import com.printing.partner.fragments.SavingsFragment
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.json.JSONObject


class DashboardActivity : BaseActivity(R.layout.activity_dashboard), View.OnClickListener {

    companion object{
      lateinit var Tips :ArrayList<Tip>
      lateinit var cat :ArrayList<Categories>
      lateinit var catTransactionedLastMonth :ArrayList<Categories>
      lateinit var catTransactionedThisMonth :ArrayList<Categories>
        lateinit var weekly_spend :String
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Tips = ArrayList()
        cat = ArrayList()
        catTransactionedLastMonth = ArrayList()
        catTransactionedThisMonth = ArrayList()
        weekly_spend ="0.0"
        getAllTips()
    }

    private fun getAllTips() {
        val jsonObject = JSONObject()
        jsonObject.put("auth_token", AppPref.User?.authToken)
        jsonObject.put("is_last_month",false.toString())

        ShowDialog()
        callApi(apiClient.getAllTips(jsonObject), false) {
            Tips = it.Tips!!
            getTransactionedCategory()
        }
    }

    private fun getTransactionedCategory() {
        val jsonObject = JSONObject()
        jsonObject.put("auth_token", AppPref.User?.authToken)
        jsonObject.put("is_last_month",true)
        callApi(apiClient.getTransactionedCategories(jsonObject), false) {
            if(it.categories != null){
                val cat = it.categories
                cat.sortWith(Comparator { lhs, rhs -> rhs.amount!!.compareTo(lhs.amount!!) })
                catTransactionedLastMonth = ArrayList()
                catTransactionedLastMonth =  cat
                getTransaction()
                Log.e("LastMonth", catTransactionedLastMonth.toString())
            }
            sneakerError(it.status!!)
        }
    }

    private fun getTransactionedCategory1() {
        val jsonObject = JSONObject()
        jsonObject.put("auth_token", AppPref.User?.authToken)
        jsonObject.put("is_last_month",false)
        callApi(apiClient.getTransactionedCategories(jsonObject), false) {
            if(it.categories != null){
                val cat = it.categories
                cat.sortWith(Comparator { lhs, rhs -> rhs.amount!!.compareTo(lhs.amount!!) })
                catTransactionedThisMonth = ArrayList()
                catTransactionedThisMonth = cat
                getWeeklySpent()
                Log.e("ThisMonth ", catTransactionedThisMonth.toString())
            }
            sneakerError(it.status!!)
        }
    }


    private fun getTransaction() {
        val jsonObject = JSONObject()
        jsonObject.put("auth_token", AppPref.User?.authToken)
        jsonObject.put("is_last_month",false)
        // jsonObject.put("code", AppPref.code)

        callApi(apiClient.getTransactions(jsonObject), false) {
            if(it.categories != null){
                cat = it.categories
                cat.sortWith(Comparator { lhs, rhs -> rhs.amount!!.compareTo(lhs.amount!!) })

                getTransactionedCategory1()
            }
            sneakerError(it.status!!)
        }
    }


    private fun getWeeklySpent() {
        val jsonObject = JSONObject()
        jsonObject.put("auth_token", AppPref.User?.authToken)

        callApi(apiClient.getWeeklySpent(jsonObject), false) {
            weekly_spend = it.weekly_spend.toString()
            initView()
            initOnClick()
            HideDialog()
        }
    }

    private fun initView() {

        bottom_bar
            .addTab(BottomBarItem(R.drawable.ic_stats, 0))
            .addTab(BottomBarItem(R.drawable.ic_home, 0))
            .addTab(BottomBarItem(R.drawable.ic_add, 0))
            .addTab(BottomBarItem(R.drawable.ic_account, 0))

        bottom_bar.selectTab(2,true)
        setFragment(AddFragment())

        bottom_bar.setOnSelectListener { position ->
            when (position) {
                0 -> {
                    setFragment(InsightFragment())
                }
                1 -> {
                    setFragment(SavingsFragment())
                }
                2 -> {
                    setFragment(AddFragment())
                }
                3 -> {
                    setFragment(MyAccountFragment())
                }
            }
        }
    }

    private fun initOnClick() {

    }

    override fun onClick(view: View) {
        when (view.id) {

        }
    }

    protected fun setFragment(fragment: Fragment) {
        val t: FragmentTransaction =
            supportFragmentManager.beginTransaction()
        t.replace(R.id.content, fragment)
        t.commit()
    }
}
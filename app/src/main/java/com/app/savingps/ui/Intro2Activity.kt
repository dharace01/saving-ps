package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.teresaholfeld.stories.StoriesProgressView
import kotlinx.android.synthetic.main.activity_intro2.*

class Intro2Activity : BaseActivity(R.layout.activity_intro2), StoriesProgressView.StoriesListener {

    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()

    }

    private fun initView() {

        progress1.progress = 50F
        progress2.progress = 70F

        stories.setStoriesCount(PROGRESS_COUNT)
        stories.setStoryDuration(5000L)
        stories?.setStoriesListener(this)

        counter = 0
        stories.startStories(counter)
        setLayout()

        layout.setOnClickListener {
            finish()
        }
    }

    private fun setLayout() {
        if (counter == 0) {
            layout1.visibility = View.VISIBLE
            layout2.visibility = View.GONE
            layout3.visibility = View.GONE
        } else if (counter == 1) {
            layout1.visibility = View.GONE
            layout2.visibility = View.VISIBLE
            layout3.visibility = View.GONE
        } else {
            layout1.visibility = View.GONE
            layout2.visibility = View.GONE
            layout3.visibility = View.VISIBLE
        }
    }

    override fun onComplete() {
        finish()
    }

    override fun onNext() {
        ++counter
        setLayout()
    }

    override fun onPrev() {
        if (counter - 1 < 0) return
        --counter
        setLayout()
    }

    override fun onDestroy() {
        // Very important !
        stories?.destroy()
        super.onDestroy()
    }

    companion object {
        private const val PROGRESS_COUNT = 3
    }
}
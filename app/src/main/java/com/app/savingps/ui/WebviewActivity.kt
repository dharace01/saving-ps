package com.app.savingps.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.app.savingps.pref.AppPref


class WebviewActivity : BaseActivity(R.layout.activity_webview) {

    var wb: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        wb = findViewById<View>(R.id.webView1) as WebView
        wb!!.settings.javaScriptEnabled = true
        wb!!.settings.loadWithOverviewMode = true
        wb!!.settings.useWideViewPort = true
        wb!!.settings.builtInZoomControls = true
        wb!!.settings.pluginState = WebSettings.PluginState.ON
        wb!!.setWebViewClient(mWebViewClient)
        wb!!.addJavascriptInterface(
            MyJavaScriptInterface(this),
            "android"
        )
        wb!!.loadUrl(
            "https://auth.truelayer.com/?response_type=c\n" +
                    "ode&client_id=savingps-19023b&scope=info\n" +
                    "%20accounts%20balance%20cards%20trans\n" +
                    "actions%20direct_debits%20standing_orders\n" +
                    "%20offline_access&redirect_uri=https://truela\n" +
                    "yer-universal.herokuapp.com&providers=uk-o\n" +
                    "b-all%20uk-oauth-all%20uk-cs-mock"
        )
    }

    var mWebViewClient: WebViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView, url: String) {
            view.loadUrl("javascript:window.android.onUrlChange(window.location.href);")
        }
    }

    internal class MyJavaScriptInterface(private val webviewActivity: Activity) {
        @JavascriptInterface
        fun onUrlChange(url: String) {
            Log.d("hydrated", "onUrlChange$url")

            if (url.startsWith("https://truelayer-universal.herokuapp.com")) {
                val uri: Uri = Uri.parse(url)
                val code: String = uri.getQueryParameter("code").toString()
                Log.d("code", "onUrlChange$code")
                AppPref.code = code
                AppPref.IsLogin = true
              //  val intent = Intent(webviewActivity, ExpensesActivity::class.java)
                val intent = Intent(webviewActivity, DashboardActivity::class.java)
                webviewActivity.startActivity(intent)

            }

        }
    }


}
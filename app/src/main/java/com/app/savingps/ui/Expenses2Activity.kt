package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_expenses.*
import kotlinx.android.synthetic.main.activity_expenses2.*
import org.jetbrains.anko.intentFor

class Expenses2Activity : BaseActivity(R.layout.activity_expenses2), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

    }

    private fun initOnClick() {
        btnSave.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSave -> {
                startActivity(intentFor<DashboardActivity>())
                finish()
            }
        }
    }
}
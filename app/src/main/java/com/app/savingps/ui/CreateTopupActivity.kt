package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_create_topup.*
import org.jetbrains.anko.intentFor


class CreateTopupActivity : BaseActivity(R.layout.activity_create_topup), View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    private val paths = arrayOf("item 1", "item 2", "item 3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

        llEnter.visibility = View.GONE
        llForm.visibility = View.VISIBLE

        val adapter = ArrayAdapter(
            this@CreateTopupActivity,
            android.R.layout.simple_spinner_item, paths
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.setOnItemSelectedListener(this)
    }

    private fun initOnClick() {
        btnEnter.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnEnter -> {
                if(llForm.visibility == View.VISIBLE){
                    llEnter.visibility = View.VISIBLE
                    llForm.visibility = View.GONE
                }else{
                    startActivity(intentFor<TopupSuccessActivity>().putExtra("msg", "Top-up\nsuccessful"))
                    finish()
                }

            }
        }
    }

    override fun onItemSelected(
        parent: AdapterView<*>?,
        v: View?,
        position: Int,
        id: Long
    ) {
        when (position) {
            0 -> {
            }
            1 -> {
            }
            2 -> {
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}
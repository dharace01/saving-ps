package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_monthly_recurring_savings.*
import org.jetbrains.anko.intentFor

class MonthlyRecurringSavingsActivity : BaseActivity(R.layout.activity_monthly_recurring_savings),
    View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    private val paths = arrayOf("item 1", "item 2", "item 3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

        val adapter = ArrayAdapter(
            this@MonthlyRecurringSavingsActivity,
            android.R.layout.simple_spinner_item, paths
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.setOnItemSelectedListener(this)
    }

    private fun initOnClick() {
        icBack.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.icBack -> {
                onBackPressed()
            }
            R.id.btnUpdate -> {
                startActivity(
                    intentFor<TopupSuccessActivity>().putExtra(
                        "msg",
                        "Settings\nupdated"
                    )
                )
                finish()
            }

        }
    }

    override fun onItemSelected(
        parent: AdapterView<*>?,
        v: View?,
        position: Int,
        id: Long
    ) {
        when (position) {
            0 -> {
            }
            1 -> {
            }
            2 -> {
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}
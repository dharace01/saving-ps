package com.app.savingps.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_reset_login_pin.*
import kotlinx.android.synthetic.main.activity_update_pin.*
import kotlinx.android.synthetic.main.activity_update_pin.otpView
import kotlinx.android.synthetic.main.activity_update_pin.otpView1
import org.jetbrains.anko.intentFor

class UpdatePinActivity : BaseActivity(R.layout.activity_update_pin) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        layout.visibility = View.VISIBLE
        layout1.visibility = View.GONE

        otpView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(s.length == 5){
                    layout1.visibility = View.VISIBLE
                    layout.visibility = View.GONE
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        otpView1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(s.length == 5){
                    otpView2.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        otpView2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(s.length == 5){
                    finish()
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        icBack.setOnClickListener {
            onBackPressed()
        }


    }
}
package com.app.savingps.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_bank_and_savings_setting.*
import kotlinx.android.synthetic.main.fragment_my_account.*
import org.jetbrains.anko.intentFor

class BankAndSavingsSettingActivity : BaseActivity(R.layout.activity_bank_and_savings_setting) , View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOnClick()
    }

    private fun initOnClick() {
        icBack.setOnClickListener(this)
        llMonthLimit.setOnClickListener(this)
        llRecurringSavings.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.icBack -> {
                onBackPressed()
            }
            R.id.llMonthLimit -> {
                startActivity(intentFor<MonthlyWithdrawalLimitActivity>())
            }

            R.id.llRecurringSavings -> {
                startActivity(intentFor<MonthlyRecurringSavingsActivity>())
            }

        }
    }
}
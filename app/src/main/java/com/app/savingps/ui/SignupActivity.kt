package com.app.savingps.ui

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.app.savingps.util.isValidEmail
import com.app.savingps.util.isValidPhoneNumber
import kotlinx.android.synthetic.main.activity_signup.*
import org.jetbrains.anko.intentFor
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class SignupActivity : BaseActivity(R.layout.activity_signup), View.OnClickListener {

    val myCalendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

    }

    val date =
        OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
            myCalendar[Calendar.YEAR] = year
            myCalendar[Calendar.MONTH] = monthOfYear
            myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
            updateLabel()
        }

    private fun updateLabel() {
        val myFormat = "MM-dd-yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tvDOB.setText(sdf.format(myCalendar.time))
    }

    private fun initOnClick() {
        tvLogin.setOnClickListener(this)
        tvDOB.setOnClickListener(this)
        btnSignUp.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        // startActivity(intentFor<ExpensesActivity>())
        when (view.id) {
            R.id.tvLogin -> startActivity(intentFor<LoginActivity>())
            R.id.tvDOB -> DatePickerDialog(
                this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()

            R.id.btnSignUp -> if (isValidate()) setRegister()
        }
    }

    private fun isValidate(): Boolean {
        return when {

            TextUtils.isEmpty(tvFname.text.toString().trim()) -> {
                tvFname.error = "Please enter First name"
                false
            }

            tvFname.text.toString().trim().length < 3 -> {
                tvFname.error = "First name must be 3 characters long!"
                false
            }

            TextUtils.isEmpty(tvSurname.text.toString().trim()) -> {
                tvSurname.error = "Please enter Surname"
                false
            }

            tvSurname.text.toString().trim().length < 3 -> {
                tvSurname.error = "Surname must be 3 characters long!"
                false
            }

            !isValidEmail(tvEmail.text.toString().trim()) -> {
                tvEmail.error = "Please enter valid email address!"
                false
            }

            TextUtils.isEmpty(tvDOB.text.toString().trim()) -> {
                tvDOB.error = "Please enter Date of Birth"
                false
            }

            !isValidPhoneNumber(tvPhone.text.toString().trim()) -> {
                tvPhone.error = "Please enter valid Phone number!"
                false
            }

            TextUtils.isEmpty(tvPostcode.text.toString().trim()) -> {
                tvPostcode.error = "Please enter Postcode!"
                false
            }

            TextUtils.isEmpty(tvAddress.text.toString().trim()) -> {
                tvAddress.error = "Please enter Address!"
                false
            }

            else -> {
                true
            }
        }
    }

    private fun setRegister() {
        val jsonObject = JSONObject()
        jsonObject.put("email", tvEmail.text)
        jsonObject.put("first_name", tvFname.text)
        jsonObject.put("sur_name", tvSurname.text)
        jsonObject.put("phone", tvPhone.text)
        jsonObject.put("post_code", tvPostcode.text)
        jsonObject.put("address", tvAddress.text)
        jsonObject.put("dob", tvDOB.text)

        callApi(apiClient.postRegistration(jsonObject), true) {
            if(it.User != null){
                startActivity(intentFor<OtpVerifyActivity>().putExtra("token", it.User.authToken))
            }
            sneakerError(it.status!!)
        }
    }
}
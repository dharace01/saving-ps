package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import com.app.savingps.util.IsPermissionsAllowedCameraAndStorage
import com.app.savingps.util.LoadImage
import com.app.savingps.util.RequestPermissionCameraAndStorage
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import kotlinx.android.synthetic.main.activity_create_pot.*
import org.jetbrains.anko.intentFor


class CreatePotActivity : BaseActivity(R.layout.activity_create_pot), View.OnClickListener,
    IPickResult {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

    }

    private fun initOnClick() {
        btnAddIcon.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        img.setOnClickListener(this)
        btnCreatePot.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.btnAddIcon, R.id.img -> {
                if (IsPermissionsAllowedCameraAndStorage()) {
                    PickImageDialog.build(PickSetup()).show(this)
                } else {
                    RequestPermissionCameraAndStorage()
                }
            }
            R.id.btnCreatePot -> {
                startActivity(intentFor<PotSuccessActivity>())
                finish()
            }
        }
    }

    override fun onPickResult(r: PickResult) {
        if (r.error == null) {
            LoadImage(r.bitmap, img)
            btnAddIcon.visibility = View.GONE
            rlImg.visibility = View.VISIBLE
            //Image path
            //r.getPath();
        } else {
            Toast.makeText(this, r.error.message, Toast.LENGTH_LONG).show()
        }
    }
}
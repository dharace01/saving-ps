package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_transfer_money.*
import kotlinx.android.synthetic.main.activity_transfer_money.btnTransferMoney
import kotlinx.android.synthetic.main.activity_transfer_money.icBack
import kotlinx.android.synthetic.main.activity_transfer_money_form.*
import org.jetbrains.anko.intentFor

class TransferMoneyFormActivity : BaseActivity(R.layout.activity_transfer_money_form),
    View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

        tvTitle.text = intent.getStringExtra("title")

        if(intent.getIntExtra("from", 0) == 0){
            btnPayNewContact.visibility = View.VISIBLE
        }else{
            btnPayNewContact.visibility = View.GONE
        }

    }

    private fun initOnClick() {
        icBack.setOnClickListener(this)
        btnTransferMoney.setOnClickListener(this)
        btnPayNewContact.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.icBack -> {
                onBackPressed()
            }
            R.id.btnTransferMoney -> {
                startActivity(
                    intentFor<TopupSuccessActivity>().putExtra(
                        "msg",
                        "Transfer\nsuccessful"
                    )
                )
                finish()
            }
            R.id.btnPayNewContact -> {
                startActivity(intentFor<TransferMoneyActivity>())
            }

        }
    }
}
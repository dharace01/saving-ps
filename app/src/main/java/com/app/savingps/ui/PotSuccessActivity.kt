package com.app.savingps.ui

import android.os.Bundle
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_pot_success.*

class PotSuccessActivity : BaseActivity(R.layout.activity_pot_success) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnOk.setOnClickListener {
            finish()
        }

    }
}
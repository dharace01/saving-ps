package com.app.savingps.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_reset_login_pin.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.json.JSONObject

class ResetLoginPinActivity : BaseActivity(R.layout.activity_reset_login_pin) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        otpView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(s.length == 6){
                    otpView1.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        otpView1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(s.length == 6){
                    callSetPinApi(s)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

    }

    private fun callSetPinApi(s: CharSequence) {
        val jsonObject = JSONObject()
        jsonObject.put("pin", s.toString())
        jsonObject.put("auth_token", intent.getStringExtra("token"))

        callApi(apiClient.postCallSetPin(jsonObject), true) {
            if(it.User != null){
                startActivity(intentFor<LoginActivity>().putExtra("phone", it.User.phone))
            }
            sneakerError(it.status!!)
        }
    }
}
package com.app.savingps.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseActivity
import kotlinx.android.synthetic.main.activity_otp_verify.*
import kotlinx.android.synthetic.main.activity_signup.*
import org.jetbrains.anko.intentFor
import org.json.JSONObject

class OtpVerifyActivity : BaseActivity(R.layout.activity_otp_verify), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()

    }

    private fun initView() {
        otpView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 6) {
                    otpVerifyApi(s)
                }
            }
            override fun afterTextChanged(s: Editable) {}
        })

    }

    private fun otpVerifyApi(s: CharSequence) {

        val jsonObject = JSONObject()
        jsonObject.put("code", s.toString())
        jsonObject.put("auth_token", intent.getStringExtra("token"))

        callApi(apiClient.postVerifyOtp(jsonObject), true) {
            if(it.User != null){
                startActivity(intentFor<SetLoginPinActivity>().putExtra("token", it.User.authToken))
            }
            sneakerError(it.status!!)
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
        }
    }
}
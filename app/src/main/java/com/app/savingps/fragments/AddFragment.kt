package com.printing.partner.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.savingps.R
import com.app.savingps.adapters.PotsAdapter
import com.app.savingps.adapters.SpendingAdapter
import com.app.savingps.base.BaseFragment
import com.app.savingps.model.SpendingData
import com.app.savingps.pref.AppPref
import com.app.savingps.ui.CreatePotActivity
import com.app.savingps.ui.DashboardActivity
import com.app.savingps.ui.DashboardActivity.Companion.cat
import com.app.savingps.ui.PotDetailActivity
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import org.jetbrains.anko.intentFor
import org.json.JSONObject
import kotlin.Comparator
import kotlin.collections.ArrayList

class AddFragment : BaseFragment(R.layout.fragment_add), View.OnClickListener {

    val listData: ArrayList<SpendingData> = ArrayList()

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {

        listData.add(
            SpendingData(
                R.drawable.ic_bills,
                70F,
                "Bills",
                "£1,055.590",
                R.color.blueStart,
                R.color.blueEnd
            )
        )
        listData.add(
            SpendingData(
                R.drawable.ic_roceries,
                50F,
                "Groceries",
                "£482.27",
                R.color.orangeStart,
                R.color.orangeEnd
            )
        )
        listData.add(
            SpendingData(
                R.drawable.ic_entertainment,
                30F,
                "Entertainment",
                "£87.62",
                R.color.pinkStart,
                R.color.pinkEnd
            )
        )

        initView()
        initOnClick()

    }



    private fun initView() {

        tvName.text = "Hi "+ (AppPref.User?.firstName ?: "")
        tvTip.setText(DashboardActivity.Tips[(0 until DashboardActivity.Tips.size-1).random()].tip)

        list.adapter = SpendingAdapter(cat).also {
            it.addAll(cat.take(3))
            view!!.list.layoutManager = LinearLayoutManager(context)
            it.setItemClickListener { view, position, image ->
                when (view.id) {
                    R.id.layout -> {

                    }
                }
            }
        }

        view!!.listPot.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        listPot.adapter = PotsAdapter().also {
            it.addAll(listData)

            it.setItemClickListener { view, position, image ->
                when (view.id) {
                    R.id.layout -> {
                        startActivity(context!!.intentFor<PotDetailActivity>())
                    }
                }
            }
        }
    }

    private fun initOnClick() {
        createPot.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.createPot -> {
                startActivity(context!!.intentFor<CreatePotActivity>())
            }
        }
    }
}
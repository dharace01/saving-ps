package com.printing.partner.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.savingps.R
import com.app.savingps.adapters.SpendingAdapter
import com.app.savingps.base.BaseFragment
import com.app.savingps.ui.DashboardActivity
import com.app.savingps.ui.Intro2Activity
import com.app.savingps.ui.SpendingDetailActivity
import kotlinx.android.synthetic.main.fragment_insight.*
import kotlinx.android.synthetic.main.fragment_insight.view.*
import org.jetbrains.anko.intentFor

class InsightFragment : BaseFragment(R.layout.fragment_insight), View.OnClickListener {

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {
        initView()
        initOnClick()
    }

    private fun initView() {

        setClick(1)
        tvTip.setText(DashboardActivity.Tips[(0 until DashboardActivity.Tips.size-1).random()].tip)
        tvSpend.setText("£"+String.format("%.2f", DashboardActivity.weekly_spend.toDouble()))

        listThisMonth.adapter = SpendingAdapter(DashboardActivity.catTransactionedThisMonth).also {
            it.addAll(DashboardActivity.catTransactionedThisMonth)
            view!!.listThisMonth.layoutManager = LinearLayoutManager(context)
            it.setItemClickListener { view, position, item ->
                when (view.id) {
                    R.id.layout -> {
                        startActivity(
                            context!!.intentFor<SpendingDetailActivity>().putExtra("img",
                                item.category?.categoryImage_url
                            ).putExtra("title", item.category?.name)
                        )
                    }
                }
            }
        }

        listLastMonth.adapter = SpendingAdapter(DashboardActivity.catTransactionedLastMonth).also {
            it.addAll(DashboardActivity.catTransactionedLastMonth)
            view!!.listLastMonth.layoutManager = LinearLayoutManager(context)
            it.setItemClickListener { view, position, item ->
                when (view.id) {
                    R.id.layout -> {
                        startActivity(
                            context!!.intentFor<SpendingDetailActivity>().putExtra("img",
                                item.category?.categoryImage_url
                            ).putExtra("title", item.category?.name)
                        )
                    }
                }
            }
        }
    }

    private fun initOnClick() {
        progress.setOnClickListener(this)
        tvLastMonth.setOnClickListener(this)
        tvThisMonth.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.progress -> {
                startActivity(context!!.intentFor<Intro2Activity>())
            }
            R.id.tvLastMonth -> {
               setClick(0)
            }
            R.id.tvThisMonth -> {
                setClick(1)
            }
        }
    }

    private fun setClick(i: Int) {

        Log.e("123 ", DashboardActivity.catTransactionedLastMonth.toString())
        Log.e("123       ", DashboardActivity.catTransactionedThisMonth.toString())

        if(i==0){
            tvLastMonth.setBackgroundResource(R.drawable.btn_dark_grey_bg)
            tvThisMonth.background = null
            listThisMonth.visibility = View.GONE
            listLastMonth.visibility = View.VISIBLE
        }else{
            tvThisMonth.setBackgroundResource(R.drawable.btn_dark_grey_bg)
            tvLastMonth.background = null
            listThisMonth.visibility = View.VISIBLE
            listLastMonth.visibility = View.GONE
        }
    }
}
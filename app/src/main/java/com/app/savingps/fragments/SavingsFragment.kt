package com.printing.partner.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.savingps.R
import com.app.savingps.adapters.SavingssAdapter
import com.app.savingps.base.BaseFragment
import com.app.savingps.ui.CreateTopupActivity
import kotlinx.android.synthetic.main.fragment_savings.*
import kotlinx.android.synthetic.main.fragment_savings.view.*
import org.jetbrains.anko.intentFor

class SavingsFragment : BaseFragment(R.layout.fragment_savings), View.OnClickListener {

    val listData: ArrayList<String> = ArrayList()

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {

        listData.add("1");
        listData.add("1");
        listData.add("1");

        initView()
        initOnClick()

    }

    private fun initView() {

        list.adapter = SavingssAdapter().also {
            it.addAll(listData)
            view!!.list.layoutManager = LinearLayoutManager(context)
            it.setItemClickListener { view, position, image ->
                when (view.id) {
                    R.id.layout -> {

                    }
                }
            }
        }

    }

    private fun initOnClick() {
        createTopup.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.createTopup -> {
                startActivity(context!!.intentFor<CreateTopupActivity>())
            }
        }
    }
}
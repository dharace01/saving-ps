package com.printing.partner.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.app.savingps.R
import com.app.savingps.base.BaseFragment
import com.app.savingps.pref.AppPref
import com.app.savingps.ui.*
import kotlinx.android.synthetic.main.fragment_my_account.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class MyAccountFragment : BaseFragment(R.layout.fragment_my_account), View.OnClickListener {

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {

        initOnClick()
    }


    private fun initOnClick() {
        llUpdateDetails.setOnClickListener(this)
        llSecurity.setOnClickListener(this)
        llBankSettings.setOnClickListener(this)
        llWithdrawMoney.setOnClickListener(this)
        btnLogout.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llUpdateDetails -> {
                startActivity(context!!.intentFor<UpdateDetailsActivity>())
            }
            R.id.llSecurity -> {
                startActivity(context!!.intentFor<UpdatePinActivity>())
            }
            R.id.llBankSettings -> {
                startActivity(context!!.intentFor<BankAndSavingsSettingActivity>())
            }
            R.id.llWithdrawMoney -> {
                startActivity(context!!.intentFor<TransferMoneyActivity>())
            }
            R.id.btnLogout -> {
                AppPref.clearPref()
                startActivity(context!!.intentFor<LoginActivity>().clearTask().newTask())

            }
        }
    }
}
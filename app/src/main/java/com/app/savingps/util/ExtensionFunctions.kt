package com.app.savingps.util

import android.Manifest.permission
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.SystemClock
import android.provider.Settings
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Log
import android.util.Patterns
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.app.savingps.R
import org.jetbrains.anko.toast
import java.util.*

const val USER_NAME = "USER_NAME"
const val INTENT_DATA = "INTENT_DATA"
const val FOR_YOU_FRAGMENT_VISIBILITY = "FOR_YOU_FRAGMENT_VISIBILITY"

const val PERMISSION_REQUEST_CODE = 200

var mLastClickTime: Long = 0

internal var SpinKitProgressDialog: Dialog? = null
internal var alertDialog: AlertDialog.Builder? = null

var Status = arrayOf("Pending", "Approved", "Rejected")

var PaymentStatus = arrayOf("Unpaid", "Paid")

fun getLongToDate(timestamp: Long, dateFormat: String, multiply: Long): String {
    val calendar = Calendar.getInstance(Locale.ENGLISH)
    calendar.timeInMillis = timestamp * multiply
    return DateFormat.format(dateFormat, calendar).toString()
}

fun Context.AvoidTwoClick(): Boolean {
    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
        return true
    } else {
        mLastClickTime = SystemClock.elapsedRealtime()
        return false
    }
}

fun isValidPhoneNumber(phoneNumber: CharSequence): Boolean {
    return if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.length >= 10) {
        Patterns.PHONE.matcher(phoneNumber).matches()
    } else false
}

fun isValidEmail(target: CharSequence?): Boolean {
    return if (target == null) {
        false
    } else {
        Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}

fun Context.isNetworkConnectionAvailable(): Boolean {
    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo
    return (activeNetwork != null && activeNetwork.isConnected)
}

fun Context.CheckInternetConnection(): Boolean {
    return if (isNetworkConnectionAvailable()) {
        Log.d("Network", "Connected")
        true
    } else {
        Log.d("Network", "Not Connected")
        CheckNetworkConnectionDialog()
        false
    }
}

fun Context.CheckNetworkConnectionDialog() {
    val builder = AlertDialog.Builder(this, R.style.DialogTheme)
    builder.setTitle(resources.getString(R.string.no_connection))
    builder.setMessage(resources.getString(R.string.turn_on_connection))
    builder.setNegativeButton(getString(R.string.dialog_ok)) { dialog, which -> dialog.dismiss() }
    val alertDialog = builder.create()
    alertDialog.show()
}

fun Context.ShowDialog() {
    // implementation 'com.github.ybq:Android-SpinKit:1.2.0'
    SpinKitProgressDialog = Dialog(this)
    SpinKitProgressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
    SpinKitProgressDialog!!.getWindow()!!
            .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    SpinKitProgressDialog!!.setContentView(R.layout.progress)
    SpinKitProgressDialog!!.getWindow()!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    SpinKitProgressDialog!!.getWindow()!!.setDimAmount(0.3f)
    SpinKitProgressDialog!!.setCancelable(false)
    val lp = WindowManager.LayoutParams()
    lp.copyFrom(SpinKitProgressDialog!!.getWindow()!!.attributes)
    lp.width = WindowManager.LayoutParams.WRAP_CONTENT
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    SpinKitProgressDialog!!.show()
    SpinKitProgressDialog!!.getWindow()!!.attributes = lp
}

fun Context.HideDialog() {
    if (SpinKitProgressDialog != null && SpinKitProgressDialog!!.isShowing()) {
        SpinKitProgressDialog!!.dismiss()
        SpinKitProgressDialog = null
    }
}

fun Context.LoadImage(imgUrl: Any, img: ImageView) {
    Glide.with(this)
            .load(imgUrl)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .into(img)
}

fun Context.LoadImageCenterCrop(imgUrl: Any, img: ImageView) {
    Glide.with(this)
            .load(imgUrl)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .centerCrop()
            .into(img)
}

fun Context.LoadImage(imgUrl: Any, img: ImageView, placeholder: Drawable, error: Drawable) {
    Glide.with(this)
            .load(imgUrl)
            .placeholder(placeholder)
            .error(error)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .centerCrop()
            .into(img)
}

fun Context.LoadCornerImage(data: Any, img: ImageView, roundedCorners: Int, placeholder: Int, error: Int) {
//    val roundedCorners = context.resources.getDimensionPixelSize(R.dimen._10sdp)
    Glide.with(this)
            .load(data)
            .placeholder(placeholder)
            .error(error)
            .transform(RoundedCorners(roundedCorners))
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .centerCrop()
            .into(img)
}

fun Context.LoadCircleImage(
        imgUrl: Any,
        img: ImageView,
        placeholder: Int,
        error: Int
) {
    Glide.with(this)
            .load(imgUrl)
            .placeholder(placeholder)
            .error(error)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .centerCrop()
            .circleCrop()
            .into(img)
}

fun Context.LogoutDialog(dialog_click: DialogInterface.OnClickListener) {
    val builder = AlertDialog.Builder(this, R.style.DialogTheme)
    builder.setTitle(resources.getString(R.string.app_name))
    builder.setMessage(resources.getString(R.string.are_you_sure_you_want_to_logout))
    builder.setNeutralButton(getString(R.string.dialog_yes), dialog_click)
    builder.setNegativeButton(getString(R.string.dialog_no)) { dialog, position -> dialog.dismiss() }
    val alertDialog = builder.create()
    alertDialog.show()
}

var options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

fun Context.ChooseImageOption(dialogInterface: DialogInterface.OnClickListener) {
    val text = TextView(this)
    text.setText(getString(R.string.choose_your_option))
    text.gravity = Gravity.CENTER
    text.setPadding(0, 40, 0, 40)
    text.textSize = 20f
    text.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
    text.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
    AlertDialog.Builder(this).setCustomTitle(text).setItems(options, dialogInterface).show()
}

fun Context.IsPermissionsAllowedCameraAndStorage(): Boolean {
    val Permission_0 = ActivityCompat.checkSelfPermission(this, permission.CAMERA)
    val Permission_1 = ActivityCompat.checkSelfPermission(this, permission.WRITE_EXTERNAL_STORAGE)
    val Permission_2 = ActivityCompat.checkSelfPermission(this, permission.READ_EXTERNAL_STORAGE)
    return Permission_0 == PackageManager.PERMISSION_GRANTED &&
            Permission_1 == PackageManager.PERMISSION_GRANTED &&
            Permission_2 == PackageManager.PERMISSION_GRANTED
}

fun Activity.RequestPermissionCameraAndStorage() {
    ActivityCompat.requestPermissions(this, arrayOf(
            permission.CAMERA,
            permission.WRITE_EXTERNAL_STORAGE,
            permission.READ_EXTERNAL_STORAGE
    ), PERMISSION_REQUEST_CODE)
}

fun Activity.RequestDontAskMeAgain() {
    val builder = AlertDialog.Builder(this)
    builder.setTitle(getString(R.string.app_name) + " " + getString(R.string.need_permissions))
    builder.setMessage(R.string.app_need_to_permissions)
    builder.setPositiveButton(R.string.app_setting) { dialog, which ->
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        this.startActivityForResult(intent, PERMISSION_REQUEST_CODE)
        toast(getString(R.string.go_to_permissions))
        dialog.dismiss()
    }
    builder.setCancelable(false)
    builder.show()
}

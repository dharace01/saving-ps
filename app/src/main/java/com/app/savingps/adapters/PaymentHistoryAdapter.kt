package com.app.savingps.adapters

import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseAdapter
import com.app.savingps.model.SpendingData
import com.app.savingps.util.LoadImage
import kotlinx.android.synthetic.main.item_pots.view.*


class PaymentHistoryAdapter :
    BaseAdapter<String>(R.layout.item_payment_history) {

    override fun setClickableView(itemView: View): List<View?> =
        listOf(itemView.layout)

    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: String,
        payloads: MutableList<Any>?
    ) {
        view.run {

        }
    }
}
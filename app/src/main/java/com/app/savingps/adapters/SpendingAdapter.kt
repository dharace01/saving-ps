package com.app.savingps.adapters

import android.graphics.Color
import android.util.Log
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseAdapter
import com.app.savingps.model.Categories
import com.app.savingps.util.LoadImage
import kotlinx.android.synthetic.main.item_spending.view.*


class SpendingAdapter(cat: ArrayList<Categories>) :
    BaseAdapter<Categories>(R.layout.item_spending) {

    val categories: ArrayList<Categories>? = cat

    override fun setClickableView(itemView: View): List<View?> =
        listOf(itemView.layout)

    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: Categories,
        payloads: MutableList<Any>?
    ) {
        view.run {

            mContext.LoadImage(
                item.category!!.categoryImage_url!!,
                img
            )

            var percentage = (item.amount?.times(100))?.div(categories?.get(0)?.amount!!)

            Log.e("per", percentage.toString())

            name.text = (item.category.name!!).toString()
            amount.text = ("£"+item.amount).toString()
            progress.setEndColor(Color.parseColor(item.category.finalColor.toString()))
            progress.setStartColor(Color.parseColor(item.category.initialColor.toString()))
            progress.setEndProgress(percentage?.toFloat()!!)
            progress.startProgressAnimation()

        }
    }
}
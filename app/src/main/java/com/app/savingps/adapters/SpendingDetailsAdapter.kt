package com.app.savingps.adapters

import android.graphics.Color
import android.view.View
import com.app.savingps.R
import com.app.savingps.base.BaseAdapter
import com.app.savingps.model.SpendingData
import com.app.savingps.model.SpendingDetailData
import com.app.savingps.util.LoadImage
import kotlinx.android.synthetic.main.item_spending.view.*


class SpendingDetailsAdapter :

    BaseAdapter<SpendingDetailData>(R.layout.item_spending_details) {

    override fun setClickableView(itemView: View): List<View?> =
        listOf(itemView.layout)

    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: SpendingDetailData,
        payloads: MutableList<Any>?
    ) {
        view.run {

            mContext.LoadImage(
                item.img!!,
                img
            )
            name.text = (item.name ?: 0).toString()
            amount.text = (item.amount ?: 0).toString()
        }
    }
}
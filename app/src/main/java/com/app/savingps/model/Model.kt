package com.app.savingps.model

import android.graphics.Color
import android.os.Parcelable
import androidx.annotation.Keep
import com.app.savingps.R
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

enum class ResponseCode constructor(val code: Int) {
    OK(200),
    BadRequest(400),
    Unauthenticated(401),
    Unauthorized(403),
    NotFound(404),
    RequestTimeOut(408),
    Conflict(409),
    InValidateData(422),
    Blocked(423),
    ForceUpdate(426),
    ServerError(500);
}

@Keep
data class BaseModelResponse<T>(
        var status: Boolean,
        val message: String = "",
        val data: T? = null
) : Serializable

@Keep
data class BaseListResponse<T>(
        var status: Boolean,
        val message: String = "",
        val data: ArrayList<T>? = null
) : Serializable


@Parcelize
data class RegistrationResponse(
    val User: User? = null,
    val status: String? = "",
    val is_code_needed:  Boolean? = false
) : Parcelable

@Parcelize
data class User(
    val id: String? = "",
    val authToken: String? = "",
    val creationDate: String? = "", //2021-02-08T15:53:05.070110
    val email: String? = "",
    val lastLoginDate: String? = "",
    val phone: String? = "",
    val updationDate: String? = "",
    val verificationCodePhone: String? = "",
    val isPhoneVerified: Boolean? = false,
    val firstName: String? = "",
    val surname: String? = "",
    val address: String? = "",
    val profilePicture_url: String? = "",
    val dob: String? = "", //"2021-08-01"
    val nationality: String? = "",
    val isAccountBalanceShow: String? = "",
) : Parcelable

@Parcelize
data class GetTransactionsData(
        val categories:ArrayList<Categories>? = null,
        val status: String? = "",
        val new_banks_transactions: String? = "",
        val new_cards_transactions: String? = ""
) : Parcelable

@Parcelize
data class Categories(
    val id: String? = "",
    val total_transactions: String? = "",
    val amount: Double? = 0.0,
    val category: Category? = null
) : Parcelable

@Parcelize
data class Category(
    val id: String? = "",
    val name: String? = "",
    val categoryImage_url: String? = "",
    val initialColor: String? = "",
    val middleColor: String? = "",
    val finalColor: String? = "",
    val isDuplicateable: String? = "",
) : Parcelable

@Parcelize
data class GetTipsData(
    val Tips:ArrayList<Tip>? = null,
) : Parcelable

@Parcelize
data class Tip(
    val tip: String? = ""
) : Parcelable

@Parcelize
data class WeeklySpentData(
    val weekly_spend: Double = 0.0,
    val count: String? = ""
) : Parcelable

@Parcelize
data class ReferenceResponse(
        val success: Boolean? = false,
        val data: ReferenceData? = null,
        val msg: String? = ""
) : Parcelable

@Parcelize
data class ReferenceData(
    val refrence_no: String? = "",
    val fname: String? = "",
    val lname: String? = "",
    val mobile: String? = "",
    val email: String? = "",
    val address_type: String? = "",
    val wing_no: String? = "",
    val house_no: String? = "",
    val society: String? = "",
    val landmark: String? = "",
    val pincode: String? = "",
    val country: String? = "",
    val state: String? = "",
    val city: String? = "",
    val upline_id: String? = "",
    val status: String? = "",
    val created_on: String? = ""
) : Parcelable

@Parcelize
data class SpendingData(
    val img: Int? = 0,
    val progress: Float = 0F,
    val name: String? = "",
    val amount: String? = "",
    val color1: Int = R.color.colorAccent,
    val color2: Int = R.color.colorAccent,
) : Parcelable

@Parcelize
data class SpendingDetailData(
    val img: Int? = 0,
    val name: String? = "",
    val amount: String? = "",
) : Parcelable



package com.app.savingps.model.api_client

import com.app.savingps.model.*
import io.reactivex.Observable
import org.json.JSONObject

class ApiClient : ApiMethod {

    var header: HashMap<String, String>? = null

    companion object {
        const val BASE_URL: String = "http://134.122.96.120:8006/"
        const val AVATAR_URL: String = "http://client.codersquads.com/mediauploads/document/"
    }

    constructor()

    private fun getApiHeader(): HashMap<String, String> {
        if (header == null) {
            header = HashMap<String, String>()
            header!!.put("Content-Type", "application/json")
        }

        return header!!
    }

    fun postReference(request: HashMap<String, String>): Observable<ReferenceResponse> {
        return RxPostRequest(
            BASE_URL.plus("api_ref_no"),
            request
        ).getObjectObservable(ReferenceResponse::class.java)
    }

    fun postRegistration(
        jsonObject: JSONObject
    ): Observable<RegistrationResponse> {
        return RxPostJsonRequest(
            BASE_URL.plus("sign_up/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(RegistrationResponse::class.java)
    }

    fun postVerifyOtp(
        jsonObject: JSONObject
    ): Observable<RegistrationResponse> {
        return RxPostJsonRequest(
            BASE_URL.plus("verify_code/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(RegistrationResponse::class.java)
    }

    fun postCallSetPin(
        jsonObject: JSONObject
    ): Observable<RegistrationResponse> {
        return RxPostJsonRequest(
            BASE_URL.plus("set_pin/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(RegistrationResponse::class.java)
    }

    fun postCallLogin(
        jsonObject: JSONObject
    ): Observable<RegistrationResponse> {
        return RxPostJsonRequest(
            BASE_URL.plus("logIn/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(RegistrationResponse::class.java)
    }

    fun postForgetPin(
        jsonObject: JSONObject
    ): Observable<RegistrationResponse> {
        return RxPostJsonRequest(
            BASE_URL.plus("forget_pin/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(RegistrationResponse::class.java)
    }

    fun getTransactions(
        jsonObject: JSONObject
    ): Observable<GetTransactionsData> {
        return RxPostJsonRequest(
            BASE_URL.plus("get_transactions/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(GetTransactionsData::class.java)
    }

    fun getAllTips(
        jsonObject: JSONObject
    ): Observable<GetTipsData> {
        return RxPostJsonRequest(
            BASE_URL.plus("get_all_tips/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(GetTipsData::class.java)
    }

    fun getTransactionedCategories(
        jsonObject: JSONObject
    ): Observable<GetTransactionsData> {
        return RxPostJsonRequest(
            BASE_URL.plus("get_transactioned_categories/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(GetTransactionsData::class.java)
    }

    fun getWeeklySpent(
        jsonObject: JSONObject
    ): Observable<WeeklySpentData> {
        return RxPostJsonRequest(
            BASE_URL.plus("get_weekly_spend/"),
            jsonObject,
            getApiHeader()
        ).getObjectObservable(WeeklySpentData::class.java)
    }
}

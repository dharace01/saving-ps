package com.app.savingps.model.api_client

import com.androidnetworking.common.Priority
import com.rx2androidnetworking.Rx2ANRequest
import com.rx2androidnetworking.Rx2AndroidNetworking
import org.json.JSONObject
import java.util.*

open class ApiMethod {
    //    companion object {
    fun RxPostJsonRequest(path: String, jsonObject: JSONObject, header: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
                .addHeaders(header)
                .addJSONObjectBody(jsonObject)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxPostRequest(path: String): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxPostRequest(path: String, request: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path) //                .addHeaders(headers)
                .addBodyParameter(request)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxPutRequest(path: String, request: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.put(path) //                .addHeaders(headers)
                .addBodyParameter(request)
                .setTag("PutRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxDeleteRequest(path: String): Rx2ANRequest {
        return Rx2AndroidNetworking.delete(path) //                .addHeaders(headers)
                .setTag("DeleteRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxPostAllDynamicRequest(path: String, request: HashMap<Any, Any>): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
                .addBodyParameter(request)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxPostDynamicRequest(path: String, request: HashMap<String, Any>): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path) //                .addHeaders(headers)
                .addBodyParameter(request)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxPostRequest(path: String, request: HashMap<String, String>, headers: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
                .addHeaders(headers)
                .addBodyParameter(request)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxGetRequest(path: String, body: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.get(path) //                .addHeaders(headers)
                .addQueryParameter(body)
                .setTag("GetRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxGetRequest(path: String, body: HashMap<String, String>, headers: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.get(path)
                .addHeaders(headers)
                .addQueryParameter(body)
                .setTag("GetRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

    fun RxDownloadRequest(path: String, dirPath: String, fileName: String): Rx2ANRequest {
        return Rx2AndroidNetworking.download(path, dirPath, fileName)
                .setTag("DownloadRequest")
                .setPriority(Priority.HIGH)
                .build()
    }
//    }
}